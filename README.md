# Django & Firebase To-Do App

This is a simple application for one user that let you add, modify and delete some messages or tasks.
It is built with Django and Firebase.

## Requirements

- Python 3

*optionnal*

- *Use a Python vitual environnement to manage your versions.*
*ex : vitualenv or miniconda*


## Install the project 

* Clone the repo

```
git clone https://gitlab.com/malandel/firebase-django-todo.git
cd firebase-django-todo

# if you have one activate your virtual environnement
conda activate myenv

# install Django && Pyrebase
pip install -r requirements.txt

python3 manage.py migrate
python3 manage.py createsuperuser

```

:warning: Remember your credentials for this superuser, you'll use them to log in your Django administration board !

* **Firebase**

-- Create an account with Firebase : https://console.firebase.google.com/u/0/ 

-- Create a user and allow authentication with email/password
> Authentication > Sign-in method
> Authentication > Users

:warning: Remember the email and password you register because you'll need them to log in your app

-- Create a Firebase app
> Home > Add an app > Web

-- Return to the home screen and create a Realtime Database
:warning: Make sure you give public rights.


## Connect your Firebase to your Django app

- Copy the file `firebase_conf_example.py` and rename it `firebase_conf.py`
- Go to your project's parameters in your Firebase dashboard
- In General parameters, scroll down to `Your Applications > SDK installation and configuration`
- Copy your informations and paste them in `firebase_conf.py` : 
-- apiKey, authDomain, projectId, storageBucket, messagingSenderId, appId.
-- You can find your databaseURL in your Realtime Database informations

## Run your app

- `python3 manage.py runserver`
- Log in with the email and password you used to create your first user in firebase
- Now you can add, delete and update messages to your app




