import pyrebase 
from django.shortcuts import render, redirect
from djangofirebase.firebase_conf import *

config = {
    'apiKey': api_key,
    'authDomain': authDomain,
    'projectId': projectId,
    'storageBucket': storageBucket,
    'messagingSenderId': messagingSenderId,
    'appId': appId,
    "databaseURL" : databaseURL
  }

firebase = pyrebase.initialize_app(config)

auth = firebase.auth()

db = firebase.database()

def signIn(request):
    return render(request, 'signIn.html')

def home(request):
    if "validate_modify" in request.POST:
        message_content = request.POST["message_content"]
        message_id = request.POST["validate_modify"]
        db.child("messages").child(message_id).update({"message": message_content})
    list_messages = getMessages()
    email = getEmail(request)
    return render(request, 'welcome.html',{"messages" : list_messages, "e":email} )

def postsign(request):
    email = request.POST.get('email')
    passw = request.POST.get('pass')
    try:
        user = auth.sign_in_with_email_and_password(email,passw)
    except:
        message = 'invalid credentials'
        return render(request,'signIn.html',{'msg':message})
    
    request.session["user_session"] = user
    
    list_messages = getMessages()
    
    return render(request, 'welcome.html',{'e':email, "messages" : list_messages})

def validate_message(request):
    if "message" in request.POST:
        message = request.POST["message"]
        data = { "message" : request.POST["message"]}
        user = request.session.get("user_session")
        user = auth.refresh(user['refreshToken'])
        db.child("messages").push(data, user['idToken'])  
    else:
        message = ""
    email = getEmail(request)
    return render(request, 'validate_message.html',{'message': message, "e" : email})

def delete_message(request):
    id_message_to_delete = request.POST['delete']
    db.child("messages").child(id_message_to_delete).remove()
    return redirect('/home/')

def modify_message(request):
    id_message_to_modify = request.POST['modify']
    list_messages = getMessages()
    email = getEmail(request)
    return render(request, 'welcome.html',{"id": id_message_to_modify, "messages" : list_messages, "e":email})


def getMessages():
    list_messages = {}
    messages = db.child("messages").get()
    for message in messages.each() or []:
        list_messages[message.key()] = message.val()
    return list_messages

def getEmail(request):
    session = request.session.get('user_session')
    return session["email"]
